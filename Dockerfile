FROM mcantillana/django_unab:3.0.1

ADD requirement.txt /code
RUN pip install --upgrade pip
RUN pip install -r requirement.txt
ADD . /code