from django.conf.urls import url
from django.urls import path, include
from . import views


book_patterns = [
    path('', views.BookListView.as_view(), name='books-list'),
    path('new', views.BookCreate.as_view(), name='book-new'),
    path('edit/<int:pk>', views.BookUpdate.as_view(), name='book-edit'),
    path('<int:pk>', views.BookDetailView.as_view(), name='book-detail'),
    path('delete/<int:pk>', views.BookDelete.as_view(), name='book-delete'),
 
]

languaje_patterns = [
    path('', views.LanguajeListView.as_view(), name='lan_list'),
    path('new', views.LanguajeCreate.as_view(), name='lan_create'),
    path('edit/<int:pk>', views.LanguajeUpdate.as_view(), name='lan_edit'),
    path('delete/<int:pk>', views.LanguajeDelete.as_view(), name='lan_delete'),
 
]
genre_patterns = [
    path('', views.GenreListView.as_view(), name='gen_list'),
    path('new', views.GenreCreate.as_view(), name='gen_create'),
    path('edit/<int:pk>', views.GenreUpdate.as_view(), name='gen_edit'),
    path('delete/<int:pk>', views.GenreDelete.as_view(), name='gen_delete'),
]

authors_patterns = [
    path('', views.AuthorListView.as_view(), name='author_list'),
    path('new', views.AuthorCreate.as_view(), name='author_new'),
    path('edit/<int:pk>', views.AuthorUpdate.as_view(), name='author_edit'),
    path('<int:pk>', views.AuthorDetailView.as_view(), name='author_detail'),
    path('delete/<int:pk>', views.AuthorDelete.as_view(), name='author_delete'),
]
bookinstance_patterns = [
    path('', views.BookInstanceListView.as_view(), name='bookinstance_list'),
    path('new', views.BookInstanceCreate.as_view(), name='bookinstance_new'),
    path('edit/<slug:id>', views.BookInstanceUpdate.as_view(), name='bookinstance_edit'),
    path('<slug:id>', views.BookInstanceDetailView.as_view(), name='bookinstance_detail'),
]

urlpatterns = [
    path('', views.index, name='index'),
    path('book/', include(book_patterns)),
    path('author/', include(authors_patterns)),
    path('genre/', include(genre_patterns)),
    path('languaje/', include(languaje_patterns)),
    path('bookinstance/', include(bookinstance_patterns)),

    path('mybooks/', views.LoanedBooksByUserListView.as_view(), name='my-borrowed'),
    
    # path('book/intance/', include(bookintance_patterns)),
]