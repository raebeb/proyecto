from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import render
from django.contrib import messages
from django.views import generic
from .models import *
from .forms import *

@login_required
def index(request):
    template = "catalog/index.html" 
    num_books=Book.objects.all().count()
    num_instances=BookInstance.objects.all().count()
    num_instances_available=BookInstance.objects.filter(status__exact='a').count()
    num_authors=Author.objects.count()

    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1

    ctx = {
        'num_books' :num_books,
        'num_instances':num_instances,
        'num_instances_available':num_instances_available,
        'num_authors':num_authors,
        'num_visits': num_visits,
    }
    return render(request, template, ctx)

""" BOOK """
class BookListView(LoginRequiredMixin,generic.ListView):
    model = Book
    queryset = Book.objects.all()
    paginate_by = 10

class BookDetailView(LoginRequiredMixin,generic.DetailView):
    model = Book
    queryset = Book.objects.all()
    template_name = 'catalog/book_detail.html'
    
class BookCreate(PermissionRequiredMixin,generic.CreateView):
    permission_required = 'catalog.add_book'
    model = Book
    form_class = BookForm
    success_url = reverse_lazy('books-list')
    template_name = 'catalog/book_create.html'

class BookUpdate(PermissionRequiredMixin,generic.UpdateView):
    permission_required = 'catalog.change_book'
    model = Book
    fields = ['title', 'author', 'summary', 'isbn', 'genre', 'languaje','photo']
    success_url = reverse_lazy('books-list')

class BookDelete(PermissionRequiredMixin,generic.DeleteView):
    permission_required = ('catalog.add_book','catalog.delete_book')
    model = Book
    success_url = reverse_lazy('books-list')


""" BOOK INSTANCE """
class LoanedBooksByUserListView(LoginRequiredMixin,generic.ListView):
    # permission_required = ('catalog.can_mark_returned', 'catalog.can_edit')
    model = BookInstance
    template_name ='catalog/bookinstance_list_borrowed_user.html'
    paginate_by = 10
    
    def get_queryset(self):
        return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='o').order_by('due_back')


""" AUTHOR """
class AuthorListView(LoginRequiredMixin,generic.ListView):
    model = Author
    queryset = Author.objects.all()
    paginate_by = 5

class AuthorDetailView(LoginRequiredMixin,generic.DetailView):
    model = Author
    queryset = Author.objects.all()
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['book_list'] = Book.objects.all()
        return context
class AuthorCreate(PermissionRequiredMixin,generic.CreateView):
    permission_required = 'catalog.add_author'
    model = Author
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']    
    success_url = reverse_lazy('author_list')
    template_name = 'catalog/author_create.html'

class AuthorUpdate(PermissionRequiredMixin,generic.UpdateView):
    permission_required = 'catalog.change_author'
    model = Author
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']    
    success_url = reverse_lazy('author_list')

class AuthorDelete(PermissionRequiredMixin,generic.DeleteView):
    permission_required = 'catalog.delete_author'
    model = Author
    success_url = reverse_lazy('author_list')

""" Languaje """
class LanguajeListView(LoginRequiredMixin,generic.ListView):
    model = Languaje
    queryset = Languaje.objects.all()

class LanguajeCreate(PermissionRequiredMixin,generic.CreateView):
    permission_required = 'catalog.add_languaje'
    model = Languaje
    fields = ['name']    
    success_url = reverse_lazy('lan_list')

class LanguajeUpdate(PermissionRequiredMixin,generic.UpdateView):
    permission_required = 'catalog.change_languaje'
    model = Languaje
    fields = ['name']    
    success_url = reverse_lazy('lan_list')

class LanguajeDelete(PermissionRequiredMixin,generic.DeleteView):
    permission_required = 'catalog.delete_languaje'
    model = Languaje
    success_url = reverse_lazy('lan_list')

""" GENRE """

class GenreListView(LoginRequiredMixin,generic.ListView):
    model = Genre
    queryset = Genre.objects.all()

class GenreCreate(PermissionRequiredMixin,generic.CreateView):
    permission_required = 'catalog.add.genre'
    model = Genre
    fields = ['name']    
    success_url = reverse_lazy('gen_list')
    template_name = 'catalog/genre_create.html'

class GenreUpdate(PermissionRequiredMixin,generic.UpdateView):
    permission_required = 'catalog.change_genre'
    model = Genre
    fields = ['name']    
    success_url = reverse_lazy('gen_list')

class GenreDelete(PermissionRequiredMixin,generic.DeleteView):
    permission_required = 'catalog.delete_genre'
    model = Genre
    success_url = reverse_lazy('gen_list')

""" Intance """
class BookInstanceListView(LoginRequiredMixin,generic.ListView):
    model = BookInstance
    queryset = BookInstance.objects.all()

class BookInstanceDetailView(LoginRequiredMixin,generic.DetailView):
    model = BookInstance
    queryset = BookInstance.objects.all()
    
class BookInstanceCreate(PermissionRequiredMixin,generic.CreateView):
    permission_required = 'catalog.add.bookinstance'
    model = BookInstance
    fields = ['book', 'imprint', 'due_back', 'borrower', 'status']    
    success_url = reverse_lazy('bookinstance_list')
    template_name = 'catalog/bookinstance_create.html'

class BookInstanceUpdate(PermissionRequiredMixin,generic.UpdateView):
    permission_required = 'catalog.change_bookinstance'
    model = BookInstance
    
    fields = ['book','imprint','due_back','borrower','status']    
    success_url = reverse_lazy('bookinstance_list')
