# App de administracion para una libreria local 📚

_Esta aplicacion tiene como objetivo brindar ayuda con el registro de arriendo de libros, ademas de realizar consultas respecto al estado de los mismos, crear nuevas instancias o eliminarlas cuando sea necesario, registrar autores y ver su descripción y libros que ha escrito_

## Comenzando 🚀

_requisitos para iniciar este proyecto_

```
python3.x
pip install django-admin
pip install django-bostrap4
pip install pillow

ó 
pip install -r requirement.txt

Tener instalado docker
```


### Para ejecutar la applicación📋

_se necesita tener instalado docker y ejecutar_

```
docker.compose up -d en la raiz del proyecto
```

### Tipos de usuarios 🔧

_Contamos con 3 tipos de usuarios_

_Super admin que es capaz de ejecutar un crud en su totalidad_
```
usuario: admin password: admin
```
_El usuario bibliotecario que es capaz de ejecutar agregar, editar y listar, pero no eliminar_
```
usuario: bibloteca password: pelicano321
```
_El usuario común que es capaz solamente de visualizar contenido_
```
usuario: pelicano  password: pajaro321
```

### Versionado 📌

Usamos [Gitlab](http://gitlab.com/) para el versionado.
nos aprovechamos de sus herramientas tales como, la creación de issue como requerimiento, además incorporamos una dashboar que la utilizamos como kanban para organizar las tareas.

### Autores ✒️

_Participantes_

* **Javier Pacheco** - *Trabajo Inicial - Documentacion* - [Javier_pxl](https://gitlab.com/Javier_pxl)
* **Francisca Osores** - *Trabajo Inicial - Documentación* - [Raebeb](https://gitlab.com/Raebeb)


### Expresiones de Gratitud 🎁

* Gracias profe, estuvo bueno el curso
