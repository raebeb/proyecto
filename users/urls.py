# users/urls.py
from django.urls import path
from .views import *

app_name = 'users'

urlpatterns = [
    path('', ListUserView.as_view(), name='users'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('<int:pk>/', UpdateUserView.as_view(), name='users_edit'),
]