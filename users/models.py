from django.db import models
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    photo = models.ImageField(upload_to='images/')
    bio = models.CharField(max_length=50)
    

    def __str__(self):
        return self.username