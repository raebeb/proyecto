from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.urls import reverse_lazy
from django.shortcuts import render
from .models import CustomUser
from .forms import UserForm
from .forms import CustomUserCreationForm

# Create your views here.
# users/views.py
# from django.views.generic.edit import CreateView


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

class UpdateUserView(UpdateView):
    model = CustomUser
    form_class = UserForm
    template_name = 'users/user_add.html'
    success_url = '/'

class ListUserView(ListView):
    model = CustomUser
    template_name = 'users/user_list.html'